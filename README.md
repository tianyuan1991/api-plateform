FusionTree_RestAPI
两个方案 1. Nodejs+express+mongoose 2. Python+Flask+PyMongo
第一版主要实现功能： 1. 用户访问权限验证 2. 数据集动态创建 3. 数据上传和获取

选择方案2进行实现

### 2018-5-10更新:
**完成**
1：实现了上述1，2，3
2：写了一个Website，套了一个壳，可以免除通过postman或者代码操作。

**不足**
1：用户账号没有与Endpoint关联，要不admin通看，要不general一个都不能看。
2：公司内部API功能差不多了，CRUD都行，就是权限需要再写一下。外部API可以考虑开始设计了。