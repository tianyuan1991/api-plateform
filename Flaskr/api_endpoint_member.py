from flask import Blueprint, request, session, jsonify
from Utils import Config, MongoConn, JSONUtils
from bson import ObjectId
from Wrapper import AuthWrapper
from datetime import datetime
import json


bp = Blueprint('api_endpoint_member', __name__, url_prefix='/api/auth')

# Set endpoint member
@bp.route('/<endpoint_name>/', methods=['POST'])
@AuthWrapper.require_api_token
@AuthWrapper.require_admin_right
def set_member_api(endpoint_name):

    auth_list = MongoConn.get_auth_col()
    ept = auth_list.find_one({'endpoint_name': endpoint_name})
    authdata = request.json
    print (ept)

    if ept is None:
        date = datetime.now()
        auth_list.insert({
                'endpoint_name': endpoint_name,
                'auth': authdata,
                'create_at_ts': date.strftime('%S'),
                'create_at_str': date.strftime('%Y-%m-%d %H:%M:%S')
            })
        msg = {'result': 'success', 'msg': 'Endpoint ' + endpoint_name + '\'s auth has been set.'}

    else:
        auth_list.update_one({'endpoint_name': endpoint_name},{'$set': {'auth': authdata}}, upsert=False)
        msg = {'result': 'success', 'msg': "Endpoint " + endpoint_name + "\'s auth has been modified"}

    return jsonify(msg)

    # get endpoint member
@bp.route('/<endpoint_name>/', methods=['GET'])
@AuthWrapper.require_api_token
# @AuthWrapper.require_admin_right
@AuthWrapper.require_role_check
def get_member_api(role,endpoint_name):
    auth_list = MongoConn.get_auth_col()
    if role == "admin" :
        ept = auth_list.find_one({'endpoint_name': endpoint_name})
    else:
        ept = auth_list.find_one({'endpoint_name': endpoint_name, 'auth.user': session['user']['username']})
    # print (ept)
    if ept is None:
        return JSONUtils.JSONEncoder().encode({'result': 'fail'})
    else:
        return JSONUtils.JSONEncoder().encode({'result': 'success', 'data': ept})