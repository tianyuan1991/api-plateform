from flask import Blueprint, request, session, jsonify
from Utils import Config, MongoConn, JSONUtils
from bson import ObjectId
from Wrapper import AuthWrapper
from datetime import datetime
import json


bp = Blueprint('api_api', __name__, url_prefix='/api/api')

# Save API into database
@bp.route('/<endpoint_name>/<collection_id>/create', methods=['POST'])
@AuthWrapper.require_api_token
# @AuthWrapper.require_admin_right
@AuthWrapper.require_access_write_check
def set_member_api(endpoint_name, collection_id):
    print ("==== Create API here =====")
    api_info = request.json
    print (api_info)

    if (api_info['endpoint_name'] == ''):
        msg = {'result': 'fail', 'msg': "Endpoint information is missing."}
    elif (api_info['collection_id'] == '') :
        msg = {'result': 'fail', 'msg': "Collection ID is missing."}
    elif (api_info['query'] == '') :
        msg = {'result': 'fail', 'msg': "Query is empty."}
    else :
        apis = MongoConn.get_apis_col()
        date = datetime.now()

        _id = apis.insert({
            'endpoint_name': api_info['endpoint_name'],
            'collection_name': api_info['collection_name'],
            'collection_id': api_info['collection_id'],
            'create_at_str': date.strftime('%Y-%m-%d %H:%M:%S'),
            'query': api_info['query'],
            'info': api_info['info']
        })
        msg = {'result': 'success', 'msg': 'Query has been saved.'}

    return jsonify(msg)

# list all the apis
@bp.route('/list', methods=['GET'])
@AuthWrapper.require_api_token
# @AuthWrapper.require_admin_right
@AuthWrapper.require_role_check
def admin_api_api_list(role):
    if (role == "admin"):
        apis = MongoConn.get_apis_col()
        api = list(apis.find())
    else:
        auth_list = MongoConn.get_auth_col()
        available_endpoints = list(auth_list.find({'auth.user': session['user']['username']}))
        tmp_endpoints = []
        for i in available_endpoints:
            tmp_endpoints.append(i['endpoint_name'])
        print(tmp_endpoints)

        apis = MongoConn.get_apis_col()
        api = list(apis.find({'endpoint_name': {'$in': tmp_endpoints}}))

    return JSONUtils.JSONEncoder().encode(api)

# remove a api
@bp.route('/<endpoint_name>/<collection_id>/<api_id>', methods=['DELETE'])
@AuthWrapper.require_api_token
@AuthWrapper.require_access_write_check
def admin_endpoint_api_remove(endpoint_name, collection_id,api_id):

    apis = MongoConn.get_apis_col()
    api = apis.find_one({'_id':ObjectId(api_id)})

    if api is not None:
        apis.delete_one({'_id':ObjectId(api_id)})
        msg = {'result':'successful','msg': "API removed."}
    else:
        msg = {'result':'failed','msg': "API does not exist."}

    return jsonify(msg)
